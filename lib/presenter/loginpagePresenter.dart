import 'package:flutter_test_1/view/View.dart';
import 'package:flutter_test_1/model/loginpageModel.dart';

class LoginPagePresenter {
  set view(LoginPageView value) {}
}

class LoginPageBasicPresenter implements LoginPagePresenter {
  LoginPageModel _model;
  LoginPageView _view;

  LoginPageBasicPresenter() {
    this._model = LoginPageModel();
  }

  @override
  void set view(LoginPageView value) {
    _view = value;
    this._view.refreshData(this._model);
  }
}
