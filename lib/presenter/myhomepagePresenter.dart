import 'package:flutter_test_1/view/View.dart';
import 'package:flutter_test_1/model/myhomepageModel.dart';

class MyHomePagePresenter {
  set view(MyHomePageView value) {}
  void buttonPlusClick() {}
}

class MyHomePageBasicPresenter implements MyHomePagePresenter {
  MyHomePageModel _model;
  MyHomePageView _view;

  MyHomePageBasicPresenter() {
    this._model = MyHomePageModel();
  }

  @override
  void set view(MyHomePageView value) {
    _view = value;
    this._view.refreshData(this._model);
  }

  @override
  void buttonPlusClick() {
    int _value1 = int.parse(this._model.controller1.text);
    int _value2 = int.parse(this._model.controller2.text);

    this._model.result = _value1 + _value2;

    this._view.refreshData(this._model);
  }
}
