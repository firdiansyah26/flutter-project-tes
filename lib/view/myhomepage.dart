import 'package:flutter/material.dart';
import 'package:flutter_test_1/model/myhomepageModel.dart';
import 'package:flutter_test_1/presenter/myhomepagePresenter.dart';
import 'package:flutter_test_1/view/View.dart';

class MyHomePage extends StatefulWidget {
  static String tag = 'myhomepage-page';

  final String title;
  final MyHomePagePresenter presenter;

  MyHomePage(this.presenter, {this.title, Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> implements MyHomePageView {
  MyHomePageModel _model;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SingleChildScrollView(
            child: ConstrainedBox(
                constraints: BoxConstraints(),
                child: Column(
                  children: <Widget>[
                    TextField(
                      keyboardType: TextInputType.number,
                      controller: _model.controller1,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Masukin no pertama'),
                    ),
                    TextField(
                      keyboardType: TextInputType.number,
                      controller: _model.controller2,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Masukin no kedua'),
                    ),
                    RaisedButton(
                      onPressed: () => this.widget.presenter.buttonPlusClick(),
                      child: Icon(Icons.add),
                    ),
                    Text(
                      "Hasil : ${_model.result}",
                      style: TextStyle(fontSize: 30.0),
                    ),
                  ],
                ))));
  }

  @override
  void refreshData(MyHomePageModel myhomepageModel) {
    setState(() {
      this._model = myhomepageModel;
    });
  }

  @override
  void initState() {
    super.initState();
    this.widget.presenter.view = this;
  }
}
