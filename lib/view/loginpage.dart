import 'package:flutter/material.dart';
import 'package:flutter_test_1/model/loginpageModel.dart';
import 'package:flutter_test_1/presenter/loginpagePresenter.dart';
import 'package:flutter_test_1/view/View.dart';
import 'package:flutter_test_1/view/myhomepage.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  final String title;
  final LoginPagePresenter presenter;

  LoginPage(this.presenter, {this.title, Key key}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> implements LoginPageView {
  final _formKey = GlobalKey<FormState>();
  LoginPageModel _model;

  /////////////////////////////
  // Notification
  /////////////////////////////

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;

  void showNotification() async {
    await _demoNotification();
  }

  Future<void> _demoNotification() async {
    var androidPlatformChannelSpesifics = AndroidNotificationDetails(
        'channelId', 'channelName', 'channelDescription',
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'Test Ticker');

    var iOSChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics = NotificationDetails(
        androidPlatformChannelSpesifics, iOSChannelSpesifics);

    await flutterLocalNotificationsPlugin.show(0, 'WAW ADA PROMO BARU BUAT KAMU ${_model.username.text}',
        'Ada Promo untuk pembelian Keripik Kebab Bang Acong', platformChannelSpesifics,
        payload: 'Coba Payload');
  }

  @override
  void initState() {
    super.initState();
    this.widget.presenter.view = this;

    initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    await Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SecondRoute()),
    );
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              await Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) => new SecondRoute(),
                ),
              );
            },
          )
        ],
      ),
    );
  }

  /////////////////////////////
  // Notification
  /////////////////////////////

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'BangAcong',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 200.0,
        child: Image.asset('asset/image/kebab_acong.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      controller: _model.username,
      validator: (value) {
        if (value.isEmpty) {
          return 'Masa iya kosong, diisi lah cuy usernamnenya';
        }
        return null;
      },
      // initialValue: '',
      decoration: InputDecoration(
          hintText: 'Username',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: _model.password,
      validator: (value) {
        if (value.isEmpty) {
          return 'diisi cuy, gak ngintip gue';
        }
        return null;
      },
      // initialValue: '',
      decoration: InputDecoration(
          hintText: 'Masukkan Password Kamu',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Material(
        // borderRadius: BorderRadius.circular(15.0),
        shadowColor: Colors.lightBlueAccent,
        elevation: 10.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 46.0,
          onPressed: () {
            if (_formKey.currentState.validate()) {
              showNotification();
              Navigator.of(context).pushNamed(MyHomePage.tag);
            } else {
              return null;
            }
          },
          color: Colors.lightBlueAccent,
          child: Text(
            'Login',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot Password',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Form(
      key: _formKey,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              Text(
                'Selamat Datang di Bang Acong',
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 48.0),
              email,
              // Padding(
              //   padding: const EdgeInsets.symmetric(vertical: 5.0),
              // ),
              SizedBox(height: 10.0),
              password,
              // Padding(
              //   padding: const EdgeInsets.symmetric(vertical: 5.0),
              // ),
              loginButton,
              SizedBox(height: 24.0),
              forgotLabel
            ],
          ),
        ),
      ),
    );
  }
  
  @override
  void refreshData(LoginPageModel loginpageModel) {
    setState(() {
      this._model = loginpageModel;
    });
  }
}

/////////////////////////////
// Notification
/////////////////////////////
class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Page'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Back'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
/////////////////////////////
// Notification
/////////////////////////////
