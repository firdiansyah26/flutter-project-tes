// import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test_1/presenter/myhomepagePresenter.dart';
import 'package:flutter_test_1/presenter/loginpagePresenter.dart';
import 'package:flutter_test_1/view/loginpage.dart';
import 'package:flutter_test_1/view/myhomepage.dart';

void main() => runApp(MyApp());

// DEMO
// class MyApp extends StatelessWidget {
//   //shortcut 'stl'
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(title: Text('Hallo Ini Flutter bang acong')),
//         body: Center(
//           child: Container(
//               child: Text(
//                 'Saya mempunyai usaha keripik kebab anjay bang acong cuy',
//                 textAlign: TextAlign.justify,
//                 style: TextStyle(color: Colors.white),
//               ),
//               color: Colors.blue,
//               width: 150,
//               height: 100),
//         ),
//       ),
//     );
//   }
// }

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   var angka = 0;

//   void hitungAngka(param) {
//     if (param == '+') {
//       setState(() {
//         angka = angka + 1;
//       });
//     } else {
//       setState(() {
//         angka = angka - 1;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('State : Keripik Kebab Bang Acong'),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text(
//                 angka.toString(),
//                 style: TextStyle(fontSize: angka.toDouble()),
//               ),
//               RaisedButton(
//                 child: Text('Tambah +'),
//                 onPressed: () => hitungAngka('+'),
//               ),
//               RaisedButton(
//                 child: Text('Kurang -'),
//                 onPressed: () => hitungAngka('-'),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// //Link = https://www.youtube.com/watch?v=QSpSKTcR44s
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   List<Widget> listWidget = [];
//   var currentIndex = 0;

//   void prosesDataList(param, idx) {
//     if (param.toString() == '+') {
//       setState(() {
//         listWidget.add(Row(
//           children: <Widget>[
//             Text(
//               'Data ke:' + currentIndex.toString(),
//               style: TextStyle(fontSize: 30),
//             )
//           ],
//         ));
//         currentIndex++;
//       });
//     } else {
//       setState(() {
//         listWidget.removeLast();
//         currentIndex--;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('List View : Keripik Kebab Bang Acong'),
//         ),
//         body: ListView(children: <Widget>[
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: <Widget>[
//               // Text(listWidget.length),
//               RaisedButton(
//                 child: Text('Tambah +'),
//                 onPressed: () => prosesDataList('+', currentIndex),
//               ),
//               RaisedButton(
//                 child: Text('Kurang -'),
//                 onPressed: () =>
//                     listWidget.length == 0 ? null : prosesDataList('-', 0),
//               )
//             ],
//           ),
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: listWidget,
//           )
//         ]),
//       ),
//     );
//   }
// }

// // Link https://www.youtube.com/watch?v=CV-l0CMiX0I&list=PLZQbl9Jhl-VACm40h5t6QMDB92WlopQmV&index=10
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   Random random = Random();

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(title: Text('Test Animated Container On Tap')),
//         body: Center(
//           child: GestureDetector(
//             onTap: () {
//               setState(() {

//               });
//             },
//             child: AnimatedContainer(
//               color: Color.fromARGB(255, random.nextInt(256),
//                   random.nextInt(256), random.nextInt(256)),
//               duration: Duration(seconds: 1),
//               width: 50.0 + random.nextInt(101),
//               height: 50.0 + random.nextInt(101),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// Link : https://www.youtube.com/watch?v=Fnrn-6Zq-Kc&list=PLZQbl9Jhl-VACm40h5t6QMDB92WlopQmV&index=11
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('Flexible Layout'),
//         ),
//         body: Column(
//           children: <Widget>[
//             Flexible(
//               child: Row(children: <Widget>[
//                 Flexible(
//                     flex: 1,
//                     child: Container(
//                       color: Colors.pink,
//                       margin: EdgeInsets.all(5),
//                     )),
//                 Flexible(
//                     flex: 1,
//                     child: Container(
//                       color: Colors.green,
//                       margin: EdgeInsets.all(5),
//                     )),
//                 Flexible(
//                     flex: 1,
//                     child: Container(
//                       color: Colors.purple,
//                       margin: EdgeInsets.all(5),
//                     ))
//               ]),
//             ),
//             Flexible(
//                 flex: 2,
//                 child: Container(
//                   color: Colors.amber,
//                   margin: EdgeInsets.all(5),
//                 )),
//             Flexible(
//                 flex: 1,
//                 child: Container(
//                   color: Colors.blue,
//                   margin: EdgeInsets.all(5),
//                 )),
//           ],
//         ),
//       ),
//     );
//   }
// }

// //Link : https://www.youtube.com/watch?v=fJNNZhK1wOw&list=PLZQbl9Jhl-VACm40h5t6QMDB92WlopQmV&index=12
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   List<Widget> listData = [];

//   _MyAppState() {
//     for (int i = 1; i <= 15; i++) {
//       listData.add(Column(
//         children: <Widget>[
//           Container(
//             margin: EdgeInsets.all(10),
//             child: Text(
//               'List Data no: ' +
//                   i.toString() +
//                   ' yang dipake di laposan tengan dari stack',
//               style: TextStyle(fontSize: 20),
//               textAlign: TextAlign.justify,
//             ),
//           )
//         ],
//       ));
//       // listData.add( Text(
//       //   'List Data no: ' +
//       //       i.toString() +
//       //       ' yang dipake di laposan tengan dari stack',
//       //   style: TextStyle(fontSize: 30),
//       // ));
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('Latihan Stack dan Align'),
//         ),
//         body: Stack(
//           children: <Widget>[
//             //background
//             Column(
//               children: <Widget>[
//                 Flexible(
//                   flex: 1,
//                   child: Row(
//                     children: <Widget>[
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.white,
//                         ),
//                       ),
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.black12,
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//                 Flexible(
//                   flex: 1,
//                   child: Row(
//                     children: <Widget>[
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.black12,
//                         ),
//                       ),
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.white,
//                         ),
//                       )
//                     ],
//                   ),
//                 )
//               ],
//             ),

//             // listview
//             ListView(
//               children: listData,
//             ),
//             // button

//             Align(
//               alignment: Alignment.bottomCenter,
//               child: RaisedButton(
//                 child: Text('Button Nih Cuy'),
//                 color: Colors.cyanAccent,
//                 onPressed: () {},
//               )
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(new LoginPageBasicPresenter(), title: 'Tes'),
    MyHomePage.tag: (context) => MyHomePage(new MyHomePageBasicPresenter(),
        title: 'Flutter Home Page ngitung'),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Coba Aplikasi Flutter MVP',
      theme: ThemeData(
        primaryColor: Colors.red,
      ),
      // home: MyHomePage(new MyHomePageBasicPresenter(),
      //     title: 'Flutter Home Page ngitung'),
      home: LoginPage(new LoginPageBasicPresenter(), title: ''),
      routes: routes,
    );
  }
}
